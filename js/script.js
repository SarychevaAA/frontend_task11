/* Задания на урок:

1) Удалить все рекламные блоки со страницы (правая часть сайта)

2) Изменить жанр фильма, поменять "комедия" на "драма"

3) Изменить задний фон постера с фильмом на изображение "bg.jpg". Оно лежит в папке img.
Реализовать только при помощи JS

4) Список фильмов на странице сформировать на основании данных из этого JS файла.
Отсортировать их по алфавиту

5) Добавить нумерацию выведенных фильмов */

'use strict';

const movieDB = {
    movies: [
        "Логан",
        "Лига справедливости",
        "Ла-ла лэнд",
        "Одержимость",
        "Скотт Пилигрим против..."
    ]
};

const adv = document.querySelectorAll('.promo__adv img');

adv.forEach((elem)=>{
    elem.remove();
})

const genre = document.querySelector('.promo__genre');
genre.textContent = 'драма';

const promoImg = document.querySelector('.promo__bg');
promoImg.style.backgroundImage = 'url("img/bg.jpg")';

const moviesList = document.querySelectorAll('.promo__interactive-item');
movieDB.movies.sort();
moviesList.forEach((item, index)=>{
    item.textContent = `${index+1}. ${movieDB.movies[index]}`;
})
